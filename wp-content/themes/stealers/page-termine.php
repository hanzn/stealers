<?php
/*
Template Name: Termine
*/
get_header(); ?>

	<?php get_template_part( 'modul_intro-image' ); ?>
	<?php get_template_part( 'modul_banderole' ); ?>

<section class="content">

	<div class="row">
		<div class="large-9 medium-8 column">
			<h2 class="chapter">Spieltermine</h2>
			
			<?php
				$dt = new DateTime();
				$dateString = $dt->format('Y-m-d H:i:s');
				query_posts(
			    array(  
			    		
			    		'post_type' => 'spiele',
			            'order'     => 'ASC',
			            'meta_key' => 'rw_date',
						'meta_value' => $dateString,
						'meta_compare' => '>',
			            'orderby'   => 'meta_value',
			            'posts_per_page' => 100
			            )
				);
				
				
				if ( have_posts() ) : while ( have_posts() ) : the_post();
			?>
			<?php get_template_part( 'modul_game_teaser' ); ?>			
				
			<?php endwhile; endif; wp_reset_query(); ?>

		</div>
		
		<div class="large-3 medium-4 column">
			<?php get_sidebar(); ?>
		</div>		
	</div>

</section>

<?php get_footer(); ?>