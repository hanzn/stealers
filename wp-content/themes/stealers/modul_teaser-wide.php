<div class="teaser wide clearfix">
			<?php if ( has_post_thumbnail( $thumbnail->ID ) ) : ?>
			<div class="imageHolder">
				<a href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail('thumbnail'); ?>
				</a>
			</div>
			<?php endif; ?>
			<div class="date"><?php the_date(); ?></div>
			<a href="<?php the_permalink(); ?>">
				<h3> <?php the_title(); ?></h3>
			</a>
			<?php 
				the_excerpt();
			?>
			<a href="<?php the_permalink(); ?>" class="teaserLink">
				weiter
			</a>
		</div>	