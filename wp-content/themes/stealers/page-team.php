<?php
/*
Template Name: Team
*/
get_header(); ?>
	
	<?php get_template_part( 'modul_intro-image' ); ?>

<?php get_template_part( 'modul_banderole' ); ?>
<section class="content">
	
	<div class="row">
		
	</div>

	<div class="row">
		<?php
		
		  if($post->post_parent)
		  	$children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0");
		  else
		  	$children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
		  if ($children) {
			
		  ?>
		  
		  <div class="large-2 medium-2 hide-for-small column">
			 <h3>Teams</h3>
		  	<?php
			wp_nav_menu( array(
				'sub_menu' => true,
				'direct_parent' => true,
				'menu_class'      => 'subnavigation',
			) ); 
			?>
		  </div>
		  
		 <?php }
			$teamSlug = get_post_meta($post->ID, 'team-slug', true); 
		 ?>
		 <div class="large-10 medium-10 column">
			
			<div class="row teamImageHolder">
				<div class="small-12 column">
					<?php the_post_thumbnail('full'); ?>
				</div>
			</div>
			 <div class="row">
				<div class="large-8 column">
				 	<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
					<?php the_content(); ?>
					<?php endwhile; ?>
					<?php endif; ?>
					
										
					
					 <?php
						 //QUERY FOR THE ROOSTER
						  query_posts( 
							  array( 
							  'post_type' => 'spieler2', 
							  'teams' => $teamSlug,
							  'order'     => 'ASC',
					          'meta_key' => 'player_name',
					           'orderby'   => 'meta_value',
							  'posts_per_page' => 40 
							  ) 
						  );
						  
						  if ( have_posts() ):
						  
						  ?>
						  
						   <h3>Roster</h3>
								
								<table class="responsive rooster fullsize">
									<thead>
										<tr>
											
											<th>#</th>
											<th>Bild</th>
											<th>Name</th>
											<th>Throw/Bat</th>
											<th>Position</th>
		<!--
											<th>Stealer seit</th>
											<th>Geburtstag</th>
		-->
										</tr>
									</thead>
									<tbody>
							
									 	
										  
										  <?php 
										 while ( have_posts() ) : the_post();
										  
										  if(rwmb_meta( 'player_throw' ) =="rechts"){
											  $throw = "R";								  
										  }else{
											  $throw = "L";
										  }
										  
										  if(rwmb_meta( 'player_bat' ) =="rechts"){
											  $bat = "R";								  
										  }else{
											   $bat = "L";
										  }
										  
										?>
								
											<tr>
											
											<td><?php echo rwmb_meta( 'player_nbr' ); ?></td>
											<td><?php the_post_thumbnail('medium'); ?></td>
											<td><?php echo rwmb_meta( 'player_forename' ); ?> <?php echo rwmb_meta( 'player_name' ); ?></td>
											<td><?php echo $throw; ?>/<?php echo $bat; ?></td>
											<td>
												<?php 
													echo rwmb_meta( 'player_primary-position-name' ); 
													if(!rwmb_meta( 'player_secondary-position-name') =="" ){
														echo ", ";
														echo rwmb_meta( 'player_secondary-position-name');
													}
												?>
											</td>
		<!-- 									<td><?php //echo rwmb_meta( 'player_entry-year' ); ?></td> -->
		<!-- 									<td><?php //echo rwmb_meta( 'player_birthday' ); ?></td> -->
										</tr>
								<?php endwhile; endif; wp_reset_query(); ?>
										</tbody>
								</table>	
								
					<hr />
					
						
				
					<?php $firstThree = array( 
							'posts_per_page' => 5, 
							'offset'           => 0,
							'tax_query' => array(
							        array(
							        'taxonomy' => 'teams',
							        'field' => 'slug',
							        'terms' => array($teamSlug)
							        )
							    )
							    
							);
							$last_three_posts = get_posts($firstThree);
							
							
						?>
						
						<h2>News zu <?php echo the_title(); ?></h2>
							
						<?php foreach ($last_three_posts as $post) : setup_postdata( $post ) ; ?>
						
							<?php get_template_part( 'modul_teaser-wide' ); ?>	
						
						<? endforeach; 
		
							wp_reset_postdata();
						?>

					
				</div>
				
				
				<div class="large-4 column">
						<?php if($teamSlug == "stealers-1" || $teamSlug == "stealers-2" ):	?>
<!-- 							<h3>Tabelle</h3> -->
						<?php endif; ?>
						<?php
/*
							if($teamSlug == "stealers-1"){
								include "http://www.baseball-softball.de/extern/standings.php?start=http://www.baseball-softball.de/extern/start.htm&end=http://www.baseball-softball.de/extern/end.htm&t=318&ht=1674&dt=sa";
							}
							
							if($teamSlug == "stealers-2"){
								include "http://www.baseball-softball.de/extern/standings.php?start=http://www.baseball-softball.de/extern/start.htm&end=http://www.baseball-softball.de/extern/end.htm&t=320&ht=1703&dt=sa";
							}
*/
							
/*
							if($teamSlug == "stealers-3"){
								include "http://www.baseball-softball.de/extern/standings.php?start=http://www.baseball-softball.de/extern/start.htm&end=http://www.baseball-softball.de/extern/end.htm&t=285&ht=1559&dt=sa";
							}
*/

						?>
						
						
					<?php
						$dt = new DateTime();
						$dateString = $dt->format('Y-m-d H:i:s');
						query_posts(
					    array(  
					    		
					    		'post_type' => 'spiele',
					            'order'     => 'ASC',
					            'meta_key' => 'rw_date',
								'meta_value' => $dateString,
								'meta_compare' => '>',
					            'orderby'   => 'meta_value',
					            'teams' => $teamSlug,
					            'posts_per_page' => 20
					            )
						);
						
						if ( have_posts() ) :
					?>
					<h3>Spieltermine</h3>
					<?php  while ( have_posts() ) : the_post() ?>
					
						<?php get_template_part( 'modul_game_teaser' ); ?>			
					
					<?php endwhile;  endif; wp_reset_query(); ?>
										
						
				
					
				</div>
				
				
				
	 </div>
			 
			 
					
					
					
		 </div>

		
		
	</div>

</section>

<?php get_footer(); ?>