<?php
/*
Template Name: News & Termine Übersicht
*/
get_header(); ?>

	<?php get_template_part( 'modul_intro-image' ); ?>

<?php get_template_part( 'modul_banderole' ); ?>

<section class="content">

	<div class="row">
		<div class="large-8 medium-6 column">
			<h2 class="chapter">News</h2>
		<?php $firstThree = array(
			'post_type' => array( 'post'),
			'posts_per_page' => 20, 
			'offset'           => 0,
			'category'         =>  ''
			);
			$last_three_posts = get_posts($firstThree);
		?>
		
		<?php foreach ($last_three_posts as $post) : setup_postdata( $post ) ; ?>
			<?php get_template_part( 'modul_teaser-wide' ); ?>		
		<? endforeach; 
			wp_reset_postdata();
		?>
		</div>
		
		<div class="large-4 medium-6 column">
			<h2 class="chapter">Spieltermine</h2>
			
			<?php
				$dt = new DateTime();
				$dateString = $dt->format('Y-m-d H:i:s');
				query_posts(
			    array(  
			    		
			    		'post_type' => 'spiele',
			            'order'     => 'ASC',
			            'meta_key' => 'rw_date',
						'meta_value' => $dateString,
						'meta_compare' => '>',
			            'orderby'   => 'meta_value',
			            )
				);
				
				
				if ( have_posts() ) : while ( have_posts() ) : the_post();
			?>
			<?php get_template_part( 'modul_game_teaser' ); ?>			
				
			<?php endwhile; endif; wp_reset_query(); ?>

		</div>
		
	</div>

</section>

<?php get_footer(); ?>