<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="mobile-web-app-capable" content="yes">
	<link rel="shortcut icon" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon/favicon.ico" />
	
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<?php if (is_single()) : ?> 
	<meta property="og:title" content="<?php the_title(); ?>" />
	<meta property="og:url" content="<?php the_permalink(); ?>" />
	<meta property="og:image" content="<?php 
		if ( has_post_thumbnail() ) {
		if (function_exists('wp_get_attachment_thumb_url')) {
				echo wp_get_attachment_thumb_url(get_post_thumbnail_id($post->ID)); 
				}
				}
				else{
					 echo esc_url( get_template_directory_uri());
					 echo '/img/opengraph.jpg';
					}
				?>" />
	<?php else : ?>  
		<meta property="og:title" content="Baseballclub Hamburg Stealers" />
		<meta property="og:url" content="http://www.stealers.de" />
		<meta property="og:image" content="<?php echo esc_url( get_template_directory_uri() ); ?>/img/opengraph.jpg" />
	<?php endif; ?>
	
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
	
	<?php wp_head(); ?>
	
	<script src="//use.typekit.net/xvy7buo.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>
	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
	
	<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/foundation.css" />
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/vendor/modernizr.js"></script>
	

	
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/responsive-tables.css" />
	
	<!-- RESPONSIVE TABLES	 -->
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/responsive-tables.js"></script>
	
	<!-- SWIPER  -->
	<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/bower_components/swiper/dist/css/swiper.min.css">
	
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo esc_url( get_template_directory_uri() ); ?>/style.css" />
	
	
	
	<script type="text/javascript">
		(function(document,navigator,standalone) {
		// prevents links from apps from oppening in mobile safari
		// this javascript must be the first script in your <head>
		if ((standalone in navigator) && navigator[standalone]) {
		var curnode, location=document.location, stop=/^(a|html)$/i;
		document.addEventListener('click', function(e) {
		curnode=e.target;
		while (!(stop).test(curnode.nodeName)) {
		curnode=curnode.parentNode;
		}
		// Condidions to do this only on links to your own app
		// if you want all links, use if('href' in curnode) instead.
		if(
		'href' in curnode && // is a link
		(chref=curnode.href).replace(location.href,'').indexOf('#') && // is not an anchor
		( !(/^[a-z\+\.\-]+:/i).test(chref) || // either does not have a proper scheme (relative links)
		chref.indexOf(location.protocol+'//'+location.host)===0 ) // or is in the same protocol and domain
		) {
		e.preventDefault();
		location.href = curnode.href;
		}
		},false);
		}
		})(document,window.navigator,'standalone');
	</script> 
	

</head>

<body>
	<div id="headWrapper">
		<header id="mainHeader">
			<div class="logoWrapper">
				<a href="/" class="logo"></a>
			</div>
			<h1>Baseballclub Hamburg Stealers e.V.</h1>
			<h2>Baseball in Hamburg seit 1985</h2>
		</header>	
	
	
		<div id="mainNavigation" class="sticky contain-to-grid">
            <nav class="top-bar" data-topbar role="navigation">
               <ul class="title-area">
					<li class="name"><a href="/" class=""><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo_wortmarke_mobil-header.png" alt="" /></a></li>
					<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
					<li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
				</ul>
                <section class="top-bar-section">
                    <?php foundation_top_bar_l(); ?>

                    <?php foundation_top_bar_r(); ?>
                </section>
            </nav>		        
		</div>
	</div>
