<?php
/*
Template Name: News
*/
get_header(); ?>

	<?php get_template_part( 'modul_intro-image' ); ?>


<?php get_template_part( 'modul_banderole' ); ?>

<section class="content">

	<div class="row">
		<div class="large-9 medium-8 column">
			<h2 class="chapter">News & Spielberichte</h2>
		<?php $firstThree = array(
			'post_type' => array( 'post'),
			'posts_per_page' => 20, 
			'offset'           => 0,
			'category'         =>  ''
			);
			$last_three_posts = get_posts($firstThree);
		?>
		
		<?php foreach ($last_three_posts as $post) : setup_postdata( $post ) ; ?>
			<?php get_template_part( 'modul_teaser-wide' ); ?>		
		<? endforeach; 
			wp_reset_postdata();
		?>
		</div>
		
		<div class="large-3 medium-4 column">
			<?php get_sidebar(); ?>
		</div>		
	</div>

</section>

<?php get_footer(); ?>