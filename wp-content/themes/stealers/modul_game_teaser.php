<a href="<?php the_permalink(); ?>" class="gameTeaser clearfix">
				<div class="dateWrapper">
					<p>
					<span class="date">
						<?php  
						$date = new DateTime(rwmb_meta( 'rw_date' )); 
						echo date_format($date, 'd.m.Y');
						?>
					</span>
					<span class="time">
						<?php  echo rwmb_meta( 'rw_timeGameOne' );?> 
									<?php  
										if(rwmb_meta('rw_timeGameTwo')){
											echo('& ');
											echo rwmb_meta( 'rw_timeGameTwo' );
										};
									?> 
									Uhr
					</span>
					</p>
				</div>
			
			<div class="teamWrapper clearfix">
				
				<div class="homeWrapper logoBox">
				 	<div class="imageWrapper">
				 	<?php
						$homeLogo = rwmb_meta( 'rw_home-logo' ); 
						echo wp_get_attachment_image( $homeLogo, 'full' ); 
				  	?>
				 	</div>
				  <p class="teamname"><?php  echo rwmb_meta( 'rw_home-name' ); ?></p>
				</div>
				
				<div class="versus"><p>vs.</p></div>
				
				<div class="guestWrapper logoBox">						   
					<div class="imageWrapper">
					<?php  
						$guestLogo = rwmb_meta( 'rw_guest-logo' ); 
						echo wp_get_attachment_image( $guestLogo, 'full' ); 
				 	?>
					</div>
					<p class="teamname"><?php  echo rwmb_meta( 'rw_guest-name' ); ?></p>
				</div>
			</div>
			
			
			</a>
