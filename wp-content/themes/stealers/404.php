<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<section class="content">
		<div class="row">
			<div class="large-12" style="height:400px; text-align: center; padding-top: 80px;">
				<h1>Foul ball!</h1>
				<p>Diese Anfrage ging daneben. Leider kann keine Seite gefunden werden.</p>
			</div>
		</div>
	</section>
		
<?php get_footer(); ?>
