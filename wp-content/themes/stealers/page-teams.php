<?php
/*
Template Name: Teams
*/
get_header(); ?>

	<?php get_template_part( 'modul_intro-image' ); ?>
	
<?php get_template_part( 'modul_banderole' ); ?>

</div>

<section class="content teams">
	


	<div class="row">
		<div class="large-8 column teamlist">	
		<?php
			
		$defaults = array(
        'depth' => 1, 
        'show_date' => '',
        'date_format' => get_option( 'date_format' ),
        'child_of' => $post->post_parent, 
        'exclude' => '',
        'title_li' => '', 
        'echo' => 0,
        'authors' => '', 
        'sort_column' => 'menu_order, post_title',
        'link_before' => '', 
        'link_after' => '', 
        'walker' => '',
		);
		
		$defaults2 = array(
        'depth' => 2, 
        'show_date' => '',
        'date_format' => get_option( 'date_format' ),
        'child_of' => $post->ID, 
        'exclude' => '',
        'title_li' => '', 
        'echo' => 0,
        'authors' => '', 
        'sort_column' => 'menu_order, post_title',
        'link_before' => '', 
        'link_after' => '', 
        'walker' => '',
		);
		
		  if($post->post_parent)
		  	$children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0&depth=1");
		  	//$children = wp_list_pages($defaults);
		  	
		  else
		  	$children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0&depth=1");
		  	//$children = wp_list_pages($defaults2);
		  	
		  if ($children) {
			
		  ?>
		  
		 
			 
		  	<?php

			wp_nav_menu( array(
				'sub_menu' => false,
				'container_class' => 'menue-item-overview',
				'direct_parent' => true,
				'menu_class'      => '',
			) ); 

			?>
		  
		  
		 <?php }
			 		 
		 ?>
		</div>
	</div>

</section>

<?php get_footer(); ?>