


window.onload = loadScript;

	//console.log('initMap');

	function loadScript() {
	  var script = document.createElement("script");
	  script.type = "text/javascript";
	  script.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyB_kzYjD2xYPS9CgNOpLt5GQ7AyTfRPC9M&sensor=false&callback=init";
	  document.body.appendChild(script);
	}




	//if($('#map').length>0){
	//	console.log('initMap');
	//	loadScript;
	// When the window has finished loading create our google map below
	       //google.maps.event.addDomListener(window, 'load', init);
	//}

function init() {
        // Basic options for a simple Google Map
        // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
        var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: 15,

          scrollwheel: false,
            /*
            navigationControl: true,
		    mapTypeControl: true,
		    scaleControl: true,
		    draggable: true,
		    disableDefaultUI: true,
*/


            // The latitude and longitude to center the map (always required)
            center: new google.maps.LatLng(53.612265, 9.953581), // HH Niendorf
            // How you would like to style the map.
            // This is where you would paste any style found on Snazzy Maps.
            styles: [
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "saturation": "14"
            },
            {
                "color": "#efede8"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape.natural.landcover",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape.natural.terrain",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "saturation": "32"
            },
            {
                "color": "#ff0000"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.attraction",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.business",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "poi.business",
        "elementType": "geometry",
        "stylers": [
            {
                "saturation": "-100"
            },
            {
                "lightness": "26"
            },
            {
                "gamma": "0.76"
            }
        ]
    },
    {
        "featureType": "poi.business",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.business",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "saturation": "35"
            },
            {
                "lightness": "-16"
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.place_of_worship",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.place_of_worship",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "poi.school",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.sports_complex",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "poi.sports_complex",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#a5c07f"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffd55c"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "hue": "#ff0000"
            },
            {
                "weight": "8.80"
            },
            {
                "invert_lightness": true
            }
        ]
    },
    {
        "featureType": "road.highway.controlled_access",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit.station.bus",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "transit.station.rail",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#0088cc"
            },
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "visibility": "off"
            },
            {
                "color": "#030303"
            }
        ]
    }
]
        };

        // Get the HTML DOM element that will contain your map
        // We are using a div with id="map" seen below in the <body>
        var mapElement = document.getElementById('map');

        // Create the Google Map using out element and options defined above
        var map = new google.maps.Map(mapElement, mapOptions);

       
        var field1Img = 'wp-content/themes/stealers/img/map-marker.png';
        var parkingImg = 'wp-content/themes/stealers/img/map-parking-marker.png';
    
		var field1LatLng = new google.maps.LatLng(53.610027, 9.951545);
		var field2LatLng = new google.maps.LatLng(53.614521, 9.954323);
		var parkingLatLng = new google.maps.LatLng(53.610619, 9.952374);
		
		
		
		var field1Marker = new google.maps.Marker({
		      position: field1LatLng,
		      map: map,
		      icon: field1Img
		});
		
		var field2Marker = new google.maps.Marker({
		      position: field2LatLng,
		      map: map,
		      icon: field1Img
		});
		
		var parkingMarker = new google.maps.Marker({
		      position: parkingLatLng,
		      map: map,
		      icon: parkingImg
		});

		
		
		google.maps.event.addListener(field1Marker, 'click', function() {
	    	langenhorstWindow.open(map,field1Marker);
	  	});
	  	

		var lhContentString = '<div id="content">'+
		 '<div id="siteNotice">'+
		 '</div>'+
	      '<h4 id="firstHeading" class="firstHeading">Ballpark</h4>'+
	      '<div id="bodyContent">'+
	      '<p>Langenhorst</p>'+
	      '</div>'+
	      '</div>';

	  var langenhorstWindow = new google.maps.InfoWindow({
	      content: lhContentString
	  });
	  
	  
	  google.maps.event.addListener(field2Marker, 'click', function() {
	    	voigtcordesWindow.open(map,field2Marker);
	  	});
	  
	  var vcContentString = '<div id="content">'+
		 '<div id="siteNotice">'+
		 '</div>'+
	      '<h4 id="firstHeading" class="firstHeading">Ballpark</h4>'+
	      '<div id="bodyContent">'+
	      '<p>Voigt-Cordes-Damm</p>'+
	      '</div>'+
	      '</div>';

	  var voigtcordesWindow = new google.maps.InfoWindow({
	      content: vcContentString
	  });
	  
	  
	  




    }
