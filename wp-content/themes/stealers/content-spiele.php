					
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="row">
	<div class="large-8 column">
		<div class="gameStageWrapper light">
							
				<div class="homeWrapper logoBox">
				 
					<?php  
						$homeLogo = rwmb_meta( 'rw_home-logo' ); 
						echo wp_get_attachment_image( $homeLogo, 'full' ); 
				  	?>
				  
				  <p class="teamname"><?php  echo rwmb_meta( 'rw_home-name' ); ?></p>
				</div>
				
				<div class="gameInfoWrapper">
					
					<p class="versus">VS.</p>
					
				 </div>
				
				<div class="guestWrapper logoBox">
				   
					<?php  
						$guestLogo = rwmb_meta( 'rw_guest-logo' ); 
						echo wp_get_attachment_image( $guestLogo, 'medium' ); 
				 	?>
					
					<p class="teamname"><?php  echo rwmb_meta( 'rw_guest-name' ); ?></p>
				</div>
		</div>
			
		<hr class="full" />
		
		<p>
			<?php the_content(); ?>
		</p>
		
		<div class="row">
			<div class="large-4 column">
				<h4>Datum:</h4>
				<p class="">
					<?php  
					$date = new DateTime(rwmb_meta( 'rw_date' )); 
					echo date_format($date, 'd.m.Y');
					?>
				</p>
			</div>
			<div class="large-4 column">
				<h4>Uhrzeit:</h4>
				<p class="">
					Spiel 1: <?php  echo rwmb_meta( 'rw_timeGameOne' );?> Uhr<br/> 
					<?php  
						if(rwmb_meta('rw_timeGameTwo')){
							echo('Spiel 2: ');
							echo rwmb_meta( 'rw_timeGameTwo');
							echo ' Uhr';
						};
					?> 
				</p>
			</div>
			
			<div class="large-4 column">
				<h4>Tickets:</h4>
				<p>
					<?php if(rwmb_meta( 'rw_adult-price')) : ?>
						<strong>Erwachsene:</strong> <?php echo rwmb_meta( 'rw_adult-price'); ?><br/>
					<?php endif; ?> 
					
					<?php if(rwmb_meta( 'rw_kids-price')) : ?>
						<strong>Kinder:</strong> <?php echo rwmb_meta( 'rw_kids-price'); ?><br/>
					<?php endif; ?> 
					
					<?php if(rwmb_meta( 'rw_special-price')) : ?>
						<strong>Ermäßigt:</strong> <?php echo rwmb_meta( 'rw_special-price'); ?><br/>
					<?php endif; ?> 
				</p>
				<p class="small">
					<?php if(rwmb_meta( 'rw_special-info')) : ?>
						<?php echo rwmb_meta( 'rw_special-info'); ?>
					<?php endif; ?> 
				</p>
			</div>
		</div>
		<div class="row">
			<div class="large-4 column">
				<h4>Ballpark</h4>
				<p>
					<strong><?php echo rwmb_meta( 'rw_ballpark-name'); ?></strong><br/>
					<?php echo rwmb_meta( 'rw_ballpark-street'); ?><br/>
					<?php echo rwmb_meta( 'rw_ballpark-zip'); ?> <?php echo rwmb_meta( 'rw_ballpark-city'); ?><br/>
				</p>
				
			</div>
			<div class="large-8 column">
				<?php if(rwmb_meta( 'rw_ballpark-mapcode')) : ?>
					<?php echo rwmb_meta( 'rw_ballpark-mapcode'); ?>
				<?php endif; ?> 
			</div>
		</div>
		
		<hr class="full" />
		
		<div class="row">
			<?php 
			// Previous/next post navigation.
			if ( is_single() ) {
				the_post_navigation( array(
					'next_text' => 
						'<span class="post-title">Nächstes Spiel</span>',
					
					'prev_text' =>
						'<span class="post-title">Vorheriges Spiel</span>',
						
					'screen_reader_text' =>(' ' ),
				) );
			}
			?>

		</div>
		
	</div>
	
	<div class="large-4 column">
		<?php get_sidebar(); ?>
	</div>
</div>

</article>