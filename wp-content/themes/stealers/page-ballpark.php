<?php
/**
Template Name: Ballpark
 */
get_header(); ?>
	
	<?php get_template_part( 'modul_intro-image' ); ?>

<?php get_template_part( 'modul_banderole' ); ?>

<section class="mapWrapper">
	<div class="row collapse no-padding">
		<div class="large-12 medium-10 medium-centered column">
			<div id="map"></div>
		</div>
	</div>
</section>

<section class="content">
	
	<div class="row">
		<div class="large-9 medium-8 column">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>		
			<?php endwhile; endif; wp_reset_query(); ?>
		</div>
		<div class="large-3 medium-4 column">
			<?php get_sidebar(); ?>
		</div>
	</div>

</section>

<?php get_footer(); ?>