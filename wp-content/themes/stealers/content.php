<?php
/**
 * The default template for displaying content

 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="row">
		<?php
			$colCount = "large-9 medium-8";
		  if($post->post_parent)
		  	$children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0");
		  else
		  	$children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
		  if ($children) {
			  $colCount = "large-7 medium-6";
		  ?>
		  
		  <div class="large-2 medium-2 hide-for-small column">
		  	<?php
			wp_nav_menu( array(
				'sub_menu' => true,
				'direct_parent' => true,
				'menu_class'      => 'subnavigation',
			) ); 
			?>
		  </div>
		  
		 <?php }
			 if ( is_single() ) {
				$colCount = "large-9 medium-8"; 
			 }			 
		 ?>
		 

	<div class="<?php echo $colCount?> column">
			
			<header class="entry-header">
				<?php if ( is_single() ) : ?>
				<div class="date"><?php the_date(); ?></div>
				<?php endif; ?>
				<?php
					if ( is_single() ) :
						the_title( '<h1 class="entry-title">', '</h1>' );
					else :
						//the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
					endif;
				?>
			</header><!-- .entry-header -->
			
			
		
			<div class="entry-content">
				<?php
					/* translators: %s: Name of current post */
					the_content( sprintf(
						__( 'Continue reading %s', '' ),
						the_title( '<span class="screen-reader-text">', '</span>', false )
					) );
				?>
			</div><!-- .entry-content -->
		
			<?php
				// Author bio.
				if ( is_single() && get_the_author_meta( 'description' ) ) :
					get_template_part( 'author-bio' );
				endif;
			?>
		
			<footer class="entry-footer">
				<?php edit_post_link( __( 'bearbeiten' ), '<span class="edit-link">', '</span>' ); ?>
			</footer><!-- .entry-footer -->

			
			<?php 
			// Previous/next post navigation.
			if ( is_single() ) {
				the_post_navigation( array(
					'next_text' => 
						'<span class="screen-reader-text">' . __( 'Nächster Beitrag') . '</span><br/>' .
						'<span class="post-title">%title</span>',
					
					'prev_text' =>
						'<span class="screen-reader-text">' . __( 'Vorheriger Beitrag' ) . '</span> <br/>' .
						'<span class="post-title">%title</span>',
						
					'screen_reader_text' =>(' ' ),
				) );
			}
			?>
	
	
	</div>
	
	<?php //if ( is_single() ) : ?>
		<div class="large-3 medium-4 column">
			<?php get_sidebar(); ?>
		</div>
	<?php //endif; ?>
	
	
</div>

</article><!-- #post-## -->
