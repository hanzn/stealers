
<?php
/**
 * The sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

?>
<section id="sidebar">

	<div class="bannerWrapper">
		<h4 class="subheadline">Die Stealers werden unterstützt von</h4>
    		<!--
		<a href="http://www.crown-tec.de/" target="_blank">
			<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/banner/crown_300.png" alt="" />
		</a>

		<a href="http://www.sparda-bank-hamburg.de/konto_und_depot/spardasportiv" target="_blank">
			<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/banner/SpardaSportiv_300.png" alt="" />
		</a>
    -->
		<a href="http://admiral-spielhalle.com/" target="_blank">
			<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/banner/admiral-300.png" alt="" />
		</a>

		<a href="http://www.tilly-gmbh.de" target="_blank">
			<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/banner/expert_tilly.png" alt="" />
		</a>

		<a href="http://www.radsport-wulff.de/" target="_blank">
			<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/banner/radsport_wulff_banner_300.jpg" alt="" />
		</a>
				<!--
		<a href="http://haupt-beratung.com/" target="_blank">
			<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/banner/mueller-Haupt-lohnsteuerberatungs_unio_300.jpg" alt="" />
		</a>

	-->



<!--
		<a href="http://www.element-of-art.de/" target="_blank">
			<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/banner/elem_of_arts_300.png" alt="" />
		</a>

-->

	</div>
</section>
