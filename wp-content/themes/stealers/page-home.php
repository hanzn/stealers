<?php
/*
Template Name: Startseite
*/
?>

<div class="row">
		<div class="large-12 column show-for-medium-up" style="margin-bottom: 30px;">
			<a href="https://www.admiralbet.de/DE/" target="_blank">
				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/banner/quer1200x180.gif" />
			</a>
		</div>
		
		<div class="large-12 column show-for-small-only" style="margin-bottom: 30px;">
			<a href="https://www.admiralbet.de/DE/" target="_blank">
				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/banner/quer320x100.gif" />
			</a>
		</div>
		
		
		
		<div class="large-8 column">
			<h2>Hamburg Stealers want you!</h2>
			<p>Die Hamburg Stealers greifen in dieser Saison mit drei Mannschaften in den Erwachsenenspielklassen an. Dazu kommt der Nachwuchs mit T-Ballern, Schülern, Jugend und den Junioren. Wir freuen uns immer über Interessierte gleich welcher Leistungsklasse, denn für jeden findet sich aktuell die passende Mannschaft!
			</p>
			<p><strong>Nur Mut, wir freuen uns auf neue Mitspieler! Schaut einfach mal vorbei.</strong></p>
			<p><a href="/der-verein/mitglied-werden" class="teaserButton"><span>Mitglied werden</span></a></p>
		</div>
		
		<div class="large-4 column">
			<h3 class="">Die Teams</h3>
			<p>Hier findest du alle Infos zu den Teams wie z.B. Trainingszeiten, Tabellen, Spielpläne etc.</p>
			
				<?php

			$defaults = array(
				'theme_location'  => '',
				'menu'            => 'teams',
				'container'       => 'div',
				'container_class' => '',
				'container_id'    => '',
				'menu_class'      => 'team-list',
				'menu_id'         => '',
				'echo'            => true,
				'fallback_cb'     => 'wp_page_menu',
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				'depth'           => 0,
				'walker'          => ''
			);
			
			wp_nav_menu( $defaults );
			
			?>
			
		</div>
	</div>
<hr />


<div class="row">
	<?php $firstThree = array( 
		'posts_per_page' => 3, 
		'offset'           => 0,
		'category'         =>  ''
		);
		$last_three_posts = get_posts($firstThree);
	?>
		
	<?php foreach ($last_three_posts as $post) : setup_postdata( $post ) ; ?>
	<div class="large-4 medium-4 column">
		<?php get_template_part( 'modul_teaser' ); ?>	
	</div>
	<? endforeach; 
		wp_reset_postdata();
	?>
</div>

<hr class="hideSmall" />

<div class="row">
	<div class="large-12 column">
		<h3 class="">Videos</h3>
		<div id="youtubeChannel" class="vertical">
		<?php 
			echo do_shortcode( '[youtube_channel]' );
		?>
		</div>
	</div>
</div>

<hr class="" />
	
<div class="row">
	<div class="large-8 column">	
	<?php $firstThree = array( 
		'posts_per_page' => 25, 
		'offset'           => 3,
		'category'         =>  ''
		);
		$last_three_posts = get_posts($firstThree);
	?>
	
	<?php foreach ($last_three_posts as $post) : setup_postdata( $post ) ; ?>
		<?php get_template_part( 'modul_teaser-wide' ); ?>		
	<? endforeach; 
		wp_reset_postdata();
	?>
	</div>
	<div class="large-4 column">
<!-- 		<h3>1. Bundesliga Nord</h3> -->
		<?php //include "http://www.baseball-softball.de/extern/standings.php?start=http://www.baseball-softball.de/extern/start.htm&end=http://www.baseball-softball.de/extern/end.htm&t=318&ht=1674&dt=sa"; ?>

		<!-- <h3>2. Bundesliga Nord</h3> -->
		<?php //include "http://www.baseball-softball.de/extern/standings.php?start=http://www.baseball-softball.de/extern/start.htm&end=http://www.baseball-softball.de/extern/end.htm&t=282&ht=1543&dt=sa"; ?>
		
<!-- 		<h3>2. Bundesliga Nord</h3> -->
		<?php //include "http://www.baseball-softball.de/extern/standings.php?start=http://www.baseball-softball.de/extern/start.htm&end=http://www.baseball-softball.de/extern/end.htm&t=320&ht=1703&dt=sa"; ?>
	</div>
</div>
