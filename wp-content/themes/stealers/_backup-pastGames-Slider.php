<?php
//PAST GAMES
			query_posts(
			    array(  
		    		'posts_per_page' => 3,
		    		'post_type' => 'spiele',
		    		'order'     => 'ASC',
		    		'meta_key' => 'rw_date',
		    		'meta_value' => $dateString,
		    		'meta_compare' => '<',
					'orderby'   => 'meta_value',
					'teams' => 'stealers-1, stealers-2'				
		            )
			);
			if($homeChecker):
			if ( have_posts() ) : while ( have_posts() ) : the_post(); 
			$date = new DateTime(rwmb_meta( 'rw_date' ));								
			?>
				<div class="swiper-slide" data-day="<?php echo date_format($date, 'd') ?>" data-month="<?php echo date_format($date, 'M') ?>">
						
							<div class="homeWrapper logoBox">
							 
								<?php  
									$homeLogo = rwmb_meta( 'rw_home-logo' ); 
									echo wp_get_attachment_image( $homeLogo, 'full' ); 
							  	?>
							  
							  <p class="teamname"><?php  echo rwmb_meta( 'rw_home-name' ); ?></p>
							</div>
							
							<div class="gameInfoWrapper">
								<p class="date">
									<?php  
									echo date_format($date, 'd.m.Y');								
									?>
								</p>
								
								<p class="time">
									<?php  
									echo date_format($date, 'H:i');
									?> Uhr
								</p>
								
								<p class="versus">VS.</p>
								
							   <a href="<?php the_permalink(); ?>" class="teaserButton"><span>DETAILS</span></a>
							</div>
							
							<div class="guestWrapper logoBox">
							   
								<?php  
									$guestLogo = rwmb_meta( 'rw_guest-logo' ); 
									echo wp_get_attachment_image( $guestLogo, 'medium' ); 
							 	?>
								
								<p class="teamname"><?php  echo rwmb_meta( 'rw_guest-name' ); ?></p>
							</div>
						
					 </div> <!-- slide END -->
			<?php endwhile; endif; endif; wp_reset_query();?>

	