<?php
add_theme_support('menus');
add_theme_support( 'post-thumbnails' );

/**
 * Register Menus
 */
register_nav_menus(array(
    'top-bar-l' => 'Left Top Bar', // registers the menu in the WordPress admin menu editor
    'top-bar-r' => 'Right Top Bar'
));


/**
 * Left top bar
 * 
 */
function foundation_top_bar_l() {
    wp_nav_menu(array( 
        'container' => false,                           // remove nav container
        'container_class' => '',           		// class of container
        'menu' => 'left',                      	        // menu name
        'menu_class' => 'top-bar-menu left',         	// adding custom nav class
        'theme_location' => 'top-bar-l',                // where it's located in the theme
        'before' => '',                                 // before each link <a> 
        'after' => '',                                  // after each link </a>
        'link_before' => '',                            // before each link text
        'link_after' => '',                             // after each link text
        'depth' => 5,                                   // limit the depth of the nav
    	'fallback_cb' => false,                         // fallback function (see below)
        'walker' => new top_bar_walker()
	));
}

/**
 * Right top bar
 */
function foundation_top_bar_r() {
    wp_nav_menu(array( 
        'container' => false,                           // remove nav container
        'container_class' => '',           		// class of container
        'menu' => 'right',                      	        // menu name
        'menu_class' => 'top-bar-menu right',         	// adding custom nav class
        'theme_location' => 'top-bar-r',                // where it's located in the theme
        'before' => '',                                 // before each link <a> 
        'after' => '',                                  // after each link </a>
        'link_before' => '',                            // before each link text
        'link_after' => '',                             // after each link text
        'depth' => 5,                                   // limit the depth of the nav
    	'fallback_cb' => false,                         // fallback function (see below)
        'walker' => new top_bar_walker()
	));
}

/**
 * Customize the output of menus for Foundation top bar
 */

class top_bar_walker extends Walker_Nav_Menu {

    function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {
        $element->has_children = !empty( $children_elements[$element->ID] );
        $element->classes[] = ( $element->current || $element->current_item_ancestor ) ? 'active' : '';
        $element->classes[] = ( $element->has_children ) ? 'has-dropdown not-click' : '';
		
        parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }
	
    function start_el( &$output, $object, $depth = 0, $args = array(), $current_object_id = 0 ) {
        $item_html = '';
        parent::start_el( $item_html, $object, $depth, $args );	
		
        //$output .= ( $depth == 0 ) ? '<li class="divider"></li>' : '';
		
        $classes = empty( $object->classes ) ? array() : (array) $object->classes;	
		
        if( in_array('label', $classes) ) {
            $output .= '<li class="divider"></li>';
            $item_html = preg_replace( '/<a[^>]*>(.*)<\/a>/iU', '<label>$1</label>', $item_html );
        }
        
	if ( in_array('divider', $classes) ) {
		$item_html = preg_replace( '/<a[^>]*>( .* )<\/a>/iU', '', $item_html );
	}
		
        $output .= $item_html;
    }
	
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $output .= "\n<ul class=\"sub-menu dropdown\">\n";
    }
    
}


// add hook
add_filter( 'wp_nav_menu_objects', 'my_wp_nav_menu_objects_sub_menu', 10, 2 );

// filter_hook function to react on sub_menu flag
function my_wp_nav_menu_objects_sub_menu( $sorted_menu_items, $args ) {
  if ( isset( $args->sub_menu ) ) {
    $root_id = 0;
    
    // find the current menu item
    foreach ( $sorted_menu_items as $menu_item ) {
      if ( $menu_item->current ) {
        // set the root id based on whether the current menu item has a parent or not
        $root_id = ( $menu_item->menu_item_parent ) ? $menu_item->menu_item_parent : $menu_item->ID;
        break;
      }
    }
    
    // find the top level parent
    if ( ! isset( $args->direct_parent ) ) {
      $prev_root_id = $root_id;
      while ( $prev_root_id != 0 ) {
        foreach ( $sorted_menu_items as $menu_item ) {
          if ( $menu_item->ID == $prev_root_id ) {
            $prev_root_id = $menu_item->menu_item_parent;
            // don't set the root_id to 0 if we've reached the top of the menu
            if ( $prev_root_id != 0 ) $root_id = $menu_item->menu_item_parent;
            break;
          } 
        }
      }
    }

    $menu_item_parents = array();
    foreach ( $sorted_menu_items as $key => $item ) {
      // init menu_item_parents
      if ( $item->ID == $root_id ) $menu_item_parents[] = $item->ID;

      if ( in_array( $item->menu_item_parent, $menu_item_parents ) ) {
        // part of sub-tree: keep!
        $menu_item_parents[] = $item->ID;
      } else if ( ! ( isset( $args->show_parent ) && in_array( $item->ID, $menu_item_parents ) ) ) {
        // not part of sub-tree: away with it!
        unset( $sorted_menu_items[$key] );
      }
    }
    
    return $sorted_menu_items;
  } else {
    return $sorted_menu_items;
  }
}


//CUSTOM POSTTYPES

register_post_type('spieler2',
    array(
    'labels' => array(
            'name'               => _x( 'Spieler', 'post type general name', 'your-plugin-textdomain' ),
			'singular_name'      => _x( 'Spieler', 'post type singular name', 'your-plugin-textdomain' ),
			'menu_name'          => _x( 'Spieler', 'admin menu', 'your-plugin-textdomain' ),
			'name_admin_bar'     => _x( 'Spieler', 'add new on admin bar', 'your-plugin-textdomain' ),
			'add_new'            => _x( 'Hinzufügen', 'book', 'your-plugin-textdomain' ),
			'add_new_item'       => __( 'Neuen Spieler anlegen', 'your-plugin-textdomain' ),
			'new_item'           => __( 'Neuer Spieler', 'your-plugin-textdomain' ),
			'edit_item'          => __( 'Spieler bearbeiten', 'your-plugin-textdomain' ),
			'view_item'          => __( 'Spieler ansehen', 'your-plugin-textdomain' ),
			'all_items'          => __( 'Alle Spieler', 'your-plugin-textdomain' ),
			'search_items'       => __( 'Spieler suchen', 'your-plugin-textdomain' ),
			'parent_item_colon'  => __( 'Parent Player:', 'your-plugin-textdomain' ),
			'not_found'          => __( 'Kein Spieler gefunden', 'your-plugin-textdomain' ),
			'not_found_in_trash' => __( 'Kein Spieler im Papierkorb gefunden', 'your-plugin-textdomain' )
             ),

  'taxonomies' => array('category'),    

  'public' => true,
  'show_ui' => true,
  'exclude_from_search' => true,
  'hierarchical' => true,
  'supports' => array( 'title', 'editor', 'thumbnail' ),
  'query_var' => true,
  'has_archive' => true
        )
  );
  
add_filter( 'rwmb_meta_boxes', 'spieler_register_meta_boxes' );

function spieler_register_meta_boxes( $meta_boxes )
	{
		$prefix = 'player_';
	
		// SPIELR INFOS
		$meta_boxes[] = array(
		'title' => 'Spielerinformation',
		'pages' => array('spieler2' ),
		'fields' => array(
				array(
				'name' => 'Nummer',
				'desc' => '',
				'id' => $prefix . 'nbr',
				'type' => 'text',
				'size' => 2
				),
				
				array(
				'name' => 'Vorname',
				'desc' => '',
				'id' => $prefix . 'forename',
				'type' => 'text'
				),
				
				array(
				'name' => 'Nachname',
				'desc' => '',
				'id' => $prefix . 'name',
				'type' => 'text'
				),
				
				array(
				'name' => 'Geburtstag',
				'desc' => '',
				'id' => $prefix . 'birthday',
				'type' => 'text'
				),
				
				array(
				'name' => 'Bei den Stealers seit',
				'desc' => 'Jahreszahl',
				'id' => $prefix . 'entry-year',
				'type' => 'text',
				'size' => 4
				),

			)
		);
		
		$meta_boxes[] = array(
		'title' => 'Arm',
		'pages' => array('spieler2' ),
		'fields' => array(
				array(
				'name' => 'Throw',
				'desc' => '',
				'id' => $prefix . 'throw',
				'type' => 'select',
				'options' => array('rechts' => 'rechts','links' => 'links', )
				),
				
				array(
				'name' => 'Bat',
				'desc' => '',
				'id' => $prefix . 'bat',
				'type' => 'select',
				'options' => array('rechts' => 'rechts','links' => 'links', )
				),

			)
		);
		
		$meta_boxes[] = array(
		'title' => 'Position',
		'pages' => array('spieler2' ),
		'fields' => array(
			array(
				'name' => 'Hauptposition Nummer',
				'desc' => 'Pitcher = 1, Catcher = 2, First = 3, Second = 4, Third = 5, Short = 6, Left = 7, Center = 8, Right = 9', 
				'id' => $prefix . 'primary-position-nbr',
				'type' => 'select',
				'options' => array(' ' => ' ', '1' => '1','2' => '2', '3' => '3', '4' => '4', '5' => '5','6' => '6','7' => '7','8' => '8','9' => '9'),
				'size' => 1

				),
				
				array(
				'name' => 'Hauptposition Name',
				'desc' => '',
				'id' => $prefix . 'primary-position-name',
				'type' => 'text'
				),
				
				array(
				'name' => 'Zweitposition Nummer',
				'desc' => '', 
				'id' => $prefix . 'secondary-position-nbr',
				'type' => 'select',
				'options' => array(' ' => ' ', '1' => '1','2' => '2', '3' => '3', '4' => '4', '5' => '5','6' => '6','7' => '7','8' => '8','9' => '9'),
				'size' => 1

				),
				
				array(
				'name' => 'Zweitposition Name',
				'desc' => '',
				'id' => $prefix . 'secondary-position-name',
				'type' => 'text'
				),
			
			)
		);


		return $meta_boxes;
} 

  
 // hook into the init action and call create_player_taxonomies when it fires
add_action( 'init', 'create_player_taxonomies', 0);

// create taxonomis, TEAM the post type "spieler2"
function create_player_taxonomies() {
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Teams', 'taxonomy general name' ),
		'singular_name'     => _x( 'Team', 'taxonomy singular name' ),
		'search_items'      => __( 'Team suchen' ),
		'all_items'         => __( 'Alle Teams' ),
		'parent_item'       => __( 'Parent Team' ),
		'parent_item_colon' => __( 'Parent Team:' ),
		'edit_item'         => __( 'Team bearbeiten' ),
		'update_item'       => __( 'Team aktualisieren' ),
		'add_new_item'      => __( 'Neues Team anlegen' ),
		'new_item_name'     => __( 'Neuer Teamname' ),
		'menu_name'         => __( 'Teams' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'teams' ),
	);

	register_taxonomy( 'teams', array( 'spieler2','spiele', 'post' ), $args );

	}



register_post_type('spiele',
    array(
    'labels' => array(
            'name'               => _x( 'Spiele', 'post type general name', 'your-plugin-textdomain' ),
			'singular_name'      => _x( 'Spiel', 'post type singular name', 'your-plugin-textdomain' ),
			'menu_name'          => _x( 'Spiele', 'admin menu', 'your-plugin-textdomain' ),
			'name_admin_bar'     => _x( 'Spiele', 'add new on admin bar', 'your-plugin-textdomain' ),
			'add_new'            => _x( 'Hinzufügen', 'book', 'your-plugin-textdomain' ),
			'add_new_item'       => __( 'Neues Spiel anlegen', 'your-plugin-textdomain' ),
			'new_item'           => __( 'Neues Spiel', 'your-plugin-textdomain' ),
			'edit_item'          => __( 'Spiel bearbeiten', 'your-plugin-textdomain' ),
			'view_item'          => __( 'Spiel ansehen', 'your-plugin-textdomain' ),
			'all_items'          => __( 'Alle Spiele', 'your-plugin-textdomain' ),
			'search_items'       => __( 'Spiel suchen', 'your-plugin-textdomain' ),
			'parent_item_colon'  => __( 'Parent Spiel:', 'your-plugin-textdomain' ),
			'not_found'          => __( 'Kein Spiel gefunden', 'your-plugin-textdomain' ),
			'not_found_in_trash' => __( 'Kein Spiel im Papierkorb gefunden', 'your-plugin-textdomain' )
             ),

  'taxonomies' => array('category'),    

  'public' => true,
  'show_ui' => true,
  'exclude_from_search' => true,
  'hierarchical' => true,
  'supports' => array( 'title', 'editor', 'thumbnail' ),
  'query_var' => true,
  'has_archive' => true
        )
  );
  
  

add_filter( 'rwmb_meta_boxes', 'spiele_register_meta_boxes' );

function spiele_register_meta_boxes( $meta_boxes )
	{
		$prefix = 'rw_';
	
		// DATUM Uhrzeit
		$meta_boxes[] = array(
		'title' => 'Datum und Uhrzeit',
		'pages' => array('spiele' ),
		'fields' => array(
				array(
				'name' => 'Datum und Uhrzeit',
				'desc' => '',
				'id' => $prefix . 'date',
				'type' => 'datetime'
				),
			)
		);
		
		// DATUM Uhrzeit
		$meta_boxes[] = array(
		'title' => 'Ballpark',
		'pages' => array('spiele' ),
		'fields' => array(
				array(
				'name' => 'Heimspiel',
				'desc' => '',
				'id' => $prefix . 'ballpark-home',
				'type' => 'checkbox list',
				'options' => array('heimspiel' => 'Heimspiel',)
				),	
				
				array(
				'name' => 'Name',
				'desc' => '',
				'id' => $prefix . 'ballpark-name',
				'type' => 'Text',
				),
				
				array(
				'name' => 'Straße',
				'desc' => '',
				'id' => $prefix . 'ballpark-street',
				'type' => 'Text',
				),
				array(
				'name' => 'Stadt',
				'desc' => '',
				'id' => $prefix . 'ballpark-city',
				'type' => 'Text',
				),
				array(
				'name' => 'PLZ',
				'desc' => '',
				'id' => $prefix . 'ballpark-zip',
				'type' => 'text',
				'size' => '5'
				),
				
				array(
				'name' => 'Google-Maps Embedcode',
				'desc' => '',
				'id' => $prefix . 'ballpark-mapcode',
				'type' => 'textarea',
				),
				
							
			)
		);
		
		$meta_boxes[] = array(
		'title' => 'Teams',
		'pages' => array('spiele' ),
		'fields' => array(
				array(
				'name' => 'Heim Name',
				'desc' => '',
				'id' => $prefix . 'home-name',
				'type' => 'Text',
				),
				array(
				'name' => 'Heim Logo',
				'desc' => '',
				'id' => $prefix . 'home-logo',
				'type' => 'file advanced',
				),
				
				array(
				'name' => 'Gast',
				'desc' => '',
				'id' => $prefix . 'guest-name',
				'type' => 'Text',
				),
				array(
				'name' => 'Gast Logo',
				'desc' => '',
				'id' => $prefix . 'guest-logo',
				'type' => 'file advanced',
				),
			)
		);

		$meta_boxes[] = array(
		'title' => 'Ticketpeise',
		'pages' => array('spiele' ),
		'fields' => array(
				array(
				'name' => 'Erwachsener',
				'desc' => '',
				'id' => $prefix . 'adult-price',
				'type' => 'Text',
				'size' => '5'
				),
				
				array(
				'name' => 'Kinder',
				'desc' => '',
				'id' => $prefix . 'kids-price',
				'type' => 'Text',
				'size' => '5'
				),
				
				array(
				'name' => 'Ermäßigt',
				'desc' => '',
				'id' => $prefix . 'special-price',
				'type' => 'Text',
				'size' => '5'
				),
				
				array(
				'name' => 'Spezielle Information',
				'desc' => 'Information zu Sonderaktionen z.B. Jersey-Day',
				'id' => $prefix . 'special-info',
				'type' => 'Textarea',
				'size' => ''
				),
			)
		);

		

		

		return $meta_boxes;
	} 


//////////////////
	
	add_filter('pre_get_posts', 'query_post_type');
	
	function query_post_type($query) {
	  if ( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {
	    $post_type = get_query_var('post_type');
		if($post_type)
		    $post_type = $post_type;
		else
		    $post_type = array('post','spieler2'); // replace cpt to your custom post type
	    $query->set('post_type',$post_type);
		return $query;
	    }
	}

?>