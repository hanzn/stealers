<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<?php get_template_part( 'modul_intro-image' ); ?>
	
	<?php if(!is_home()){
			get_template_part( 'modul_banderole' ); 
			}
	?>
	
	<?php
		if(is_home()){
			$homeChecker = true;
		}
	?>
	
	<?php if($homeChecker): ?>
<section class="gameStageWrapper">
	<div class="row">
		<header class="banderoleWrapper">
			<div class="banderole">
				<h3>Next game</h3>
			</div>
		</header>
		<div class="large-12 large-centered ">
			<div class="swiper-container">
				 <div class="swiper-wrapper clearfix">
		
		<?php
		$dt = new DateTime();
		$dateString = $dt->format('Y-m-d H:i:s');
		?>

		
				
			<?php
//UPCOMMING GAMES
			query_posts(
			    array(  
		    		'posts_per_page' => 7,
		    		'post_type' => 'spiele',
		    		'order'     => 'ASC',
		    		'meta_key' => 'rw_date',
		    		'meta_value' => $dateString,
		    		'meta_compare' => '>',
					'orderby'   => 'meta_value',
					'teams' => 'stealers-1, stealers-2'				
		            )
			);
			
			$gameCount = 0;
			if ( have_posts() ) : while ( have_posts() ) : the_post(); 
			$date = new DateTime(rwmb_meta( 'rw_date' ));
			?>
				<div class="swiper-slide" data-day="<?php echo date_format($date, 'd') ?>" data-month="<?php echo date_format($date, 'M') ?>" data-count="<?php echo $gameCount; ?>">
						
							<div class="homeWrapper logoBox">
							 
								<?php  
									$homeLogo = rwmb_meta( 'rw_home-logo' ); 
									echo wp_get_attachment_image( $homeLogo, 'full' ); 
							  	?>
							  
							  <p class="teamname"><?php  echo rwmb_meta( 'rw_home-name' ); ?></p>
							</div>
							
							<div class="gameInfoWrapper">
								<p class="date">
									<?php  
									echo date_format($date, 'd.m.Y');								
									?>
								</p>
								
								<p class="time">
									<?php  
									echo date_format($date, 'H:i');
									?> Uhr
								</p>
								
								<p class="versus">VS.</p>
								
							   <a href="<?php the_permalink(); ?>" class="teaserButton"><span>DETAILS</span></a>
							</div>
							
							<div class="guestWrapper logoBox">
							   
								<?php  
									$guestLogo = rwmb_meta( 'rw_guest-logo' ); 
									echo wp_get_attachment_image( $guestLogo, 'medium' ); 
							 	?>
								
								<p class="teamname"><?php  echo rwmb_meta( 'rw_guest-name' ); ?></p>
							</div>
						
					 </div> <!-- slide END -->
			
			<?php $gameCount++; ?>
			<?php  endwhile; endif;  wp_reset_query();?>
	
	
	
 			</div> <!-- swiperWrapper END -->
 			
 			 <!-- Add Arrows -->
 			 <div class="swiper-button-next swiper-button-white"></div>
 			 <div class="swiper-button-prev swiper-button-white"></div>
 			 
 			  <!-- Add Pagination -->
 			  <div class="swiper-pagination"></div>
 		</div><!-- swiperContainer END -->
 		
 		
		</div>
	</div>
	</section>
	<?php endif; ?>
	
	<section class="content">

		
		<?php if(is_home()): ?>
			<?php get_template_part( 'page-home' ); ?>
		<?php endif; ?>
		<?php if(!is_home()): ?>
		<?php
			// Start the loop.
			while ( have_posts() ) : the_post();
				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'content', get_post_format() );
			// End the loop.
			endwhile;			
		?>
		<?php endif; ?>
	
		
	</section>

<?php get_footer(); ?>
