	<footer id="mainFooter">
		<div class="logo">
			<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo_wortmarke.png" alt="" />
		</div>
		<div class="row">
			<div class="large-12 column titel-line">
				<p>Deutscher Meister 2000   ·   DBV-Pokalsieger 2000   ·   CEB-Pokal Sieger 2000   ·   Deutscher Vizemeister: 1994, 1995, 1997 </p>
				<a href="/impressum-2/">Impressum</a>
			</div>
		</div>
	</footer>
	

	
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/vendor/jquery.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/foundation.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/google-maps.js"></script>
    
	<!-- SWIPER	 -->
   
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/bower_components/swiper/dist/js/swiper.jquery.min.js"></script>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/swiper.js"></script>

    
    
    <script>
      $(document).foundation();
    </script>
  <?php wp_footer(); ?>
  
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60831318-1', 'auto');
  ga('send', 'pageview');

</script>
  </body>
</html>