<?php
/**
 * In dieser Datei werden die Grundeinstellungen für WordPress vorgenommen.
 *
 * Zu diesen Einstellungen gehören: MySQL-Zugangsdaten, Tabellenpräfix,
 * Secret-Keys, Sprache und ABSPATH. Mehr Informationen zur wp-config.php gibt es
 * auf der {@link http://codex.wordpress.org/Editing_wp-config.php wp-config.php editieren}
 * Seite im Codex. Die Informationen für die MySQL-Datenbank bekommst du von deinem Webhoster.
 *
 * Diese Datei wird von der wp-config.php-Erzeugungsroutine verwendet. Sie wird ausgeführt,
 * wenn noch keine wp-config.php (aber eine wp-config-sample.php) vorhanden ist,
 * und die Installationsroutine (/wp-admin/install.php) aufgerufen wird.
 * Man kann aber auch direkt in dieser Datei alle Eingaben vornehmen und sie von
 * wp-config-sample.php in wp-config.php umbenennen und die Installation starten.
 *
 * @package WordPress
 */

/**  MySQL Einstellungen - diese Angaben bekommst du von deinem Webhoster. */
/**  Ersetze database_name_here mit dem Namen der Datenbank, die du verwenden möchtest. */
define('DB_NAME', 'stealers-website');

/** Ersetze username_here mit deinem MySQL-Datenbank-Benutzernamen */
define('DB_USER', 'root');

/** Ersetze password_here mit deinem MySQL-Passwort */
define('DB_PASSWORD', '');

/** Ersetze localhost mit der MySQL-Serveradresse */
define('DB_HOST', 'localhost');

/** Der Datenbankzeichensatz der beim Erstellen der Datenbanktabellen verwendet werden soll */
define('DB_CHARSET', 'utf8');

/** Der collate type sollte nicht geändert werden */
define('DB_COLLATE', '');

/**#@+
 * Sicherheitsschlüssel
 *
 * Ändere jeden KEY in eine beliebige, möglichst einzigartige Phrase.
 * Auf der Seite {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * kannst du dir alle KEYS generieren lassen.
 * Bitte trage für jeden KEY eine eigene Phrase ein. Du kannst die Schlüssel jederzeit wieder ändern,
 * alle angemeldeten Benutzer müssen sich danach erneut anmelden.
 *
 * @seit 2.6.0
 */
define('AUTH_KEY',         'kKNA3|-B}do3/>%:eX?RIQjoNC)1.j-qXWlD4y41mM=)<A|U.HL0|oJH<MU/n54l');
define('SECURE_AUTH_KEY',  'IMx&{F1$YZfhY|8P|+>_Ko/jv:X},o<oYUCN3>-gsS3pzSv{6kK<I~S9`/S|7*|@');
define('LOGGED_IN_KEY',    'ZgM-Ze(X!YBO(IjCfBjo2csCS[.}Lk4,ZyX9Vti{d|e7dh*g~9c9@CqYcV&crh}.');
define('NONCE_KEY',        '>9&tTWb9[Gn+gI,a4&cn{-?z<ekg@9P6t]oqzPq&s37:qeB+Y-;Qj$L{@@g0VO$Z');
define('AUTH_SALT',        'y+`>h4U0<*YEM+os*Nl6n*KrAo_Y~686b%8O-<kqQ~40J.k`U3U@uE 02;XGY?Pb');
define('SECURE_AUTH_SALT', 'ql;l2nX09Q:#}xM|_A^@XB$,C-=[r;+o5RgJLgNIaYVK)PH?[]~H?Nq&>1+xmQHt');
define('LOGGED_IN_SALT',   '~IWf;|>fKJ lM/NU>uA}dUOzaucJin]39xF5m<R}`6UU,Z*h),iO21P^|#XD3#zh');
define('NONCE_SALT',       '3V>HdS{ ?p(wC9D=d~*.u7j [R>FUT]7znKp-p#jXYmW)8@C#(nJAh{*-By=A6K$');

/**#@-*/

/**
 * WordPress Datenbanktabellen-Präfix
 *
 *  Wenn du verschiedene Präfixe benutzt, kannst du innerhalb einer Datenbank
 *  verschiedene WordPress-Installationen betreiben. Nur Zahlen, Buchstaben und Unterstriche bitte!
 */
$table_prefix  = 'wp_stealers_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
